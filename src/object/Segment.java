package object;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import mathematics.Point;
import mathematics.Vector3;
import render.Window;

public class Segment {
	
	protected Vector3 a;
	protected Vector3 b;
	
	protected ArrayList<Point> tab1;
	protected  ArrayList<Point> tab2;
	protected  ArrayList<Point> tab3;
	
	public Segment(Vector3 m_a, Vector3 m_b) {
		this.a = m_a;
		this.b = m_b;
	}
	
	public Segment() {
		this.a = new Vector3();
		this.b = new Vector3();
	}
	
	public void calculPositionSegment(Vector3 m) {
		Vector3 ab = b.add(a.scalarMultiply(-1));
		Vector3 am = m.add(a.scalarMultiply(-1));
		double det = ab.getX()*am.getY() - am.getX()*ab.getY();
		if(det < 0) {
			System.out.println("Au dessus");
		}else if(det > 0){
			System.out.println("En dessous");
		}else {
			double dot = ab.dotProduct(am);
			if(dot > 0) {
				if(b.getX() - a.getX() < m.getX() - a.getX()) {
					System.out.println("A droite");
				}else {
					System.out.println("Sur le segment");
				}
			}else if(dot < 0) {
				System.out.println("A gauche");
			}else{
				System.out.println("Sur le point A");
			}
		}
	}
	
	public static void testPosition(Graphics g) {
		Vector3 a = new Vector3(200,320,0);
		Vector3 b = new Vector3(300,300,0);
		Vector3 m = new Vector3(250,250,0);
		Segment ab = new Segment(a,b);
		
		g.setColor(Color.MAGENTA);
		g.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
		g.fillOval((int) a.getX()-2, (int) a.getY()-2, 4, 4);
		g.fillOval((int) b.getX()-2, (int) b.getY()-2, 4, 4);
		g.setColor(Color.CYAN);
		g.drawLine((int) m.getX(), (int) m.getY(), (int) m.getX(), (int) m.getY());
		g.fillOval((int) m.getX()-2, (int) m.getY()-2, 4, 4);
		
		ab.calculPositionSegment(m);
	}
	
	public static void enveloppeConvexe(ArrayList<Vector3> points, Graphics g) {
		for (int i = 0; i < points.size(); i++) {
			for (int j = i+1; j < points.size(); j++) {
				boolean is = true;
                int side = 0;
				for (int k = 0; k < points.size(); k++) {
					Vector3 ik = new Vector3(points.get(k).getX() - points.get(i).getX(), points.get(k).getY() - points.get(i).getY(), 0);
					Vector3 ij = new Vector3(points.get(j).getX() - points.get(i).getX(), points.get(j).getY() - points.get(i).getY(), 0);
					
					double det = ik.getX() * ij.getY() - ik.getY() * ij.getX();
                    if (side == 0 && det > 0 || side == 1 && det > 0){
                        side = 1;
                    } else if (side == 0 && det < 0 || side == 2 && det < 0){
                        side = 2;
                    } else if (points.get(i) != points.get(k) && points.get(j) != points.get(k)){
                        is = false;
                        break;
                    }
				}
				if(is) {
					g.drawLine((int) points.get(i).getX(), (int) points.get(i).getY(), (int) points.get(j).getX(), (int) points.get(j).getY());
				}
			}
		}
	}
	
	public static void testEnveloppe(Graphics g) {
		Vector3 a = new Vector3(200,320,0);
		Vector3 b = new Vector3(300,300,0);
		Vector3 m = new Vector3(250,250,0);
		Vector3 c = new Vector3(350,150,0);
		Vector3 d = new Vector3(320,100,0);
		Vector3 e = new Vector3(300,200,0);
		Vector3 f = new Vector3(100,150,0);
		
		g.setColor(Color.BLUE);
		g.fillOval((int) a.getX()-2, (int) a.getY()-2, 4, 4);
		g.fillOval((int) b.getX()-2, (int) b.getY()-2, 4, 4);
		g.fillOval((int) m.getX()-2, (int) m.getY()-2, 4, 4);
		g.fillOval((int) c.getX()-2, (int) c.getY()-2, 4, 4);
		g.fillOval((int) d.getX()-2, (int) d.getY()-2, 4, 4);
		g.fillOval((int) e.getX()-2, (int) e.getY()-2, 4, 4);
		g.fillOval((int) f.getX()-2, (int) f.getY()-2, 4, 4);
		ArrayList<Vector3> array = new ArrayList<Vector3>();
		array.add(a);
		array.add(b);
		array.add(m);
		array.add(c);
		array.add(d);
		array.add(e);
		array.add(f);
		Segment.enveloppeConvexe(array, g);
	}
	
	public void setDroite(Graphics g, Vector3 a, Vector3 b, Color c, int tab) {
		g.setColor(c);
		createPoint(a,g);
		createPoint(b,g);
		
		if (tab == 1) {
			this.tab1 = drawLine(g, (int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
		}else if (tab == 2){
			this.tab2 = drawLine(g, (int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
		}else if (tab == 3) {
			this.tab3 = drawLine(g, (int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
		}
	}
	
	public static void createPoint(Vector3 p, Graphics g) {
		g.fillOval((int) p.getX()-2, (int) p.getY()-2, 4, 4);
	}
	
	public void deplacementVertical(Vector3 droite, Graphics g, int WIDTH, int HEIGHT, int pas) {
		if (droite.getY() < Window.HEIGHT) {
			droite.setY(droite.getY()+pas);
			g.setColor(Color.RED);
			g.drawLine(WIDTH, (int) droite.getY(), -WIDTH,  (int) droite.getY());

			
			Boolean b1 = false;
			double x1 = 0;
			Boolean b2 = false;
			double x2 = 0;
			Boolean b3 = false;
			double x3 = 0;
			
			for (int i = 0; i < this.tab1.size(); i++) {
				if (this.tab1.get(i).getY() == droite.getY()) {
					b1 = true;
					x1 = this.tab1.get(i).getX();
				}
			}
			
			for (int i = 0; i < this.tab2.size(); i++) {
				if (this.tab2.get(i).getY() == droite.getY()) {
					b2 = true;
					x2 = this.tab2.get(i).getX();
				}
			}
			
			for (int i = 0; i < this.tab3.size(); i++) {
				if (this.tab3.get(i).getY() == droite.getY()) {
					b3 = true;
					x3 = this.tab3.get(i).getX();
				}
			}
			
			ArrayList<Double> arr = new ArrayList<Double>();
			if (b1 == true) {
				arr.add(x1);
			}
			if (b2 == true) {
				arr.add(x2);
			}
			if (b3 == true) {
				arr.add(x3);
			}
			Collections.sort(arr);
			
			String str = "";
			
			for (int i = 0; i < arr.size(); i++) {
				if (arr.get(i) == x1) {
					str += "Droite Noire, ";
				}
				if (arr.get(i) == x2) {
					str += "Droite Bleu, ";
				}
				if (arr.get(i) == x3) {
					str += "Droite Magenta, ";
				}
			}
			
			g.setColor(Color.BLACK);
			g.drawString(str, 50, 50);
			
		}
		
			
	}
	
	 public static ArrayList<Point> drawLine(Graphics g, int x1, int y1, int x2, int y2) {
		 	ArrayList<Point> array = new ArrayList<Point>();
	        int x, y;
	        int dx, dy;
	        int incx, incy;
	        int balance;

	        if (x2 >= x1) {
	            dx = x2 - x1;
	            incx = 1;
	        }else {
	            dx = x1 - x2;
	            incx = -1;
	        }

	        if (y2 >= y1) {
	            dy = y2 - y1;
	            incy = 1;
	        }else {
	            dy = y1 - y2;
	            incy = -1;
	        }

	        x = x1;
	        y = y1;

	        if (dx >= dy) {
	            dy <<= 1;
	            balance = dy - dx;
	            dx <<= 1;
	            while (x != x2) {
	            	g.drawLine(x,y,x,y);
		            Point p = new Point(x,y,0);
		            array.add(p);
	                if (balance >= 0) {
	                    y += incy;
	                    balance -= dx;
	                }
	                balance += dy;
	                x += incx;
	            }
	            g.drawLine(x,y,x,y);
	            Point p = new Point(x,y,0);
	            array.add(p);
	        }else {
	            dx <<= 1;
	            balance = dx - dy;
	            dy <<= 1;
	            while (y != y2) {
	            	g.drawLine(x,y,x,y);
		            Point p = new Point(x,y,0);
		            array.add(p);
	                if (balance >= 0) {
	                    x += incx;
	                    balance -= dy;
	                }
	                balance += dx;
	                y += incy;
	            }
	            g.drawLine(x,y,x,y);
	            Point p = new Point(x,y,0);
	            array.add(p);
	        }
	        return array;
	    }
	 
	 
	 //-------------------------------------------JARVIS
	 
	    private boolean CCW(Point p, Point q, Point r)
	    {
	        int val = (int) ((q.getY() - p.getY()) * (r.getX() - q.getX()) - (q.getX() - p.getX()) * (r.getY() - q.getY()));
	 
	         if (val >= 0)
	             return false;
	         return true;
	    }
	 
	    
	    public ArrayList<Point> Jarvis(ArrayList<Point> points)
	    {
	        int n = points.size();    
	        
	        ArrayList<Point> next=new ArrayList<>();
	 
	        int leftMost = 0;
	        for (int i = 1; i < n; i++)
	            if (points.get(i).getX() < points.get(leftMost).getX())
	                leftMost = i;
	        int p = leftMost;
	        int q;
	        
	        next.add(points.get(p));
	        do
	        {
	            q = (p + 1) % n;
	            for (int i = 0; i < n; i++)
	              if (CCW(points.get(p), points.get(i), points.get(q)))
	                 q = i;
	 
	            next.add(points.get(p));
	            p = q; 
	        } while (p != leftMost);
	 
	        return next;
	    }
	 
	 
	
	public Vector3 getA() {
		return a;
	}

	public void setA(Vector3 a) {
		this.a = a;
	}

	public Vector3 getB() {
		return b;
	}

	public void setB(Vector3 b) {
		this.b = b;
	}
	
}


