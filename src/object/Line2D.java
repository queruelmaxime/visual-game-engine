package object;

import mathematics.*;

public class Line2D {
	
	protected Point origin;
	protected Point end;
	protected Vector3 delta;
	
	public Line2D(Point m_origin, Point m_end) {//FORME DIRECTE
		this.origin = m_origin;
		this.delta = new Vector3(m_end.getX()-m_origin.getX(),m_end.getY()-m_origin.getY(),m_end.getZ()-m_origin.getZ());
		this.end = m_end;
	}

	public Line2D(Point m_origin, Vector3 m_delta) {//FORME PARAMETRE
		this.origin = m_origin;
		this.delta = m_delta;
		this.end = new Point(m_origin.getX()+m_delta.getX(),m_origin.getX()+m_delta.getY(),m_origin.getX()+m_delta.getZ());
	}
	
}
