package object;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

import mathematics.*;
import render.FileReader;

public class Polygon {
	
	protected ArrayList<Triangle> listTriangle;
	protected ArrayList<Point> listVertex;
	
	protected Point position;
	protected double distance;

	protected double[] rotate;
	protected double[] scale;
	protected Matrix rotationMaxtrix;
	
	public Boolean LERP;
	public Boolean SLERP;
	
	protected Color color = Color.RED;

	public Polygon() {
		setValue();
	}
	
	public Polygon(Color c, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ) throws IOException {
		setValue();
		this.LERP = false;
		this.SLERP = false;
		this.scale[0] = scale;
		this.scale[1] = scale;
		this.scale[2] = scale;
		position.setX(posX); 
		position.setY(posY); 
		position.setZ(posZ);
		this.rotate[0] = rotX;
		this.rotate[1] = rotY;
		this.rotate[2] = rotZ;
		this.color = c;
		if (posZ <= 0) {
			distance = -posZ;
		}else {
			distance = posZ;
		}
		
		rotationMaxtrix = setMatrix();
	}

	public Polygon(String path, Color c, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ) throws IOException {
		setValue();
		this.LERP = false;
		this.SLERP = false;
		FileReader file = new FileReader();
		file.readFile(this, path, c);
		this.scale[0] = scale;
		this.scale[1] = scale;
		this.scale[2] = scale;
		position.setX(posX); 
		position.setY(posY); 
		position.setZ(posZ);
		this.rotate[0] = rotX;
		this.rotate[1] = rotY;
		this.rotate[2] = rotZ;
		this.color = c;
		distance = posZ;
		rotationMaxtrix = setMatrix();
	}
	
	public Polygon(String path) throws IOException {
		setValue();
		FileReader file = new FileReader();
		file.readFile(this, path, this.color);
	}
	
	public Matrix setMatrix() {
		Matrix rot = new Matrix(4);
		rot = Matrix.rotateX(this.getRotateX());
		rot = rot.multiply(Matrix.rotateY(this.getRotateY()));
		rot = rot.multiply(Matrix.rotateZ(this.getRotateZ()));
		return rot;
	}
	
	private void setValue() {
		this.listVertex = new ArrayList<Point>();
		this.listTriangle = new ArrayList<Triangle>();
		this.position = new Point(0,0,0);
		
		this.rotate = new double[3];
		this.rotate[0] = 0;
		this.rotate[1] = 0;
		this.rotate[2] = 0;
		
		this.scale = new double[3];
		this.scale[0] = 1;
		this.scale[1] = 1;
		this.scale[2] = 1;
	}
	
	public void addTriangle(Triangle t) {
		this.listTriangle.add(t);
		this.listVertex.add(t.firstVertex);
		this.listVertex.add(t.secondVertex);
		this.listVertex.add(t.thirdVertex);
	}
	
	public Triangle recupTriangle(int index) {
		Triangle t;
		t = listTriangle.get(index/3);
		return t;
	}

	public ArrayList<Triangle> getListTriangle() {
		return listTriangle;
	}

	public void setListTriangle(ArrayList<Triangle> listTriangle) {
		this.listTriangle = listTriangle;
	}

	public ArrayList<Point> getListVertex() {
		return listVertex;
	}

	public void setListVertex(ArrayList<Point> listVertex) {
		this.listVertex = listVertex;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public double[] getRotate() {
		return rotate;
	}

	public void setRotate(double[] rotate) {
		this.rotate = rotate;
	}

	public double[] getScale() {
		return scale;
	}

	public void setScale(double[] scale) {
		this.scale = scale;
	}

	public void setRotateX(double x) {
		this.rotate[0] = x;
	}
	
	public double getRotateX() {
		return this.rotate[0];
	}
	
	public void setRotateY(double y) {
		this.rotate[1] = y;
	}
	
	public double getRotateY() {
		return this.rotate[1];
	}
	
	public void setRotateZ(double z) {
		this.rotate[2] = z;
	}
	
	public double getRotateZ() {
		return this.rotate[2];
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public Matrix getRotationMaxtrix() {
		return rotationMaxtrix;
	}

	public void setRotationMaxtrix(Matrix rotationMaxtrix) {
		this.rotationMaxtrix = rotationMaxtrix;
	}
}
