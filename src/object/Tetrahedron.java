package object;

import java.awt.Color;
import java.io.IOException;

import mathematics.Point;

public class Tetrahedron extends Polygon {
	
	public Tetrahedron() {
		super();
		add();
	}
	
	public Tetrahedron(Color c, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ) throws IOException {
		super(c,scale,posX,posY,posZ,rotX,rotY,rotZ);
		add();
	}
	
	private void add() {
		//DEFINE ALL 6 POINTS
		Point p1 = new Point(1,0,0);
		Point p2 = new Point(0,1,0);
		Point p3 = new Point(0,0,1);
		Point p4 = new Point(0,0,0);
		
		Triangle t1 = new Triangle(p2,p4,p3,this.color);
		this.getListTriangle().add(t1);
		this.getListVertex().add(p2);
		this.getListVertex().add(p4);
		this.getListVertex().add(p3);
		
		Triangle t2 = new Triangle(p4,p2,p1,this.color);
		this.getListTriangle().add(t2);
		this.getListVertex().add(p4);
		this.getListVertex().add(p2);
		this.getListVertex().add(p1);
		
		Triangle t3 = new Triangle(p3,p1,p2,this.color);
		this.getListTriangle().add(t3);
		this.getListVertex().add(p3);
		this.getListVertex().add(p1);
		this.getListVertex().add(p2);
		
		Triangle t4 = new Triangle(p1,p3,p4,this.color);
		this.getListTriangle().add(t4);
		this.getListVertex().add(p1);
		this.getListVertex().add(p3);
		this.getListVertex().add(p4);
		
		
	}

}
