package object;

import mathematics.*;

public class Plan {

	protected Vector3 e1;
	protected Vector3 e3;
	
	protected Vector3 normale;
	
	public Plan(Triangle t) {
		this.e1 = t.firstEdge;
		this.e3 = t.thirdEdge;
		this.normale = (e3.crossProduct(e1)).scalarDivision(e3.crossProduct(e1).magnitude());
	}

	public Vector3 getE1() {
		return e1;
	}

	public void setE1(Vector3 e1) {
		this.e1 = e1;
	}

	public Vector3 getE3() {
		return e3;
	}

	public void setE3(Vector3 e3) {
		this.e3 = e3;
	}

	public Vector3 getNormale() {
		return normale;
	}

	public void setNormale(Vector3 normale) {
		this.normale = normale;
	}
	
}
