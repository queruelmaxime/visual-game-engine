package object;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

import mathematics.Point;

public class Cylindre extends Polygon {
	
	protected double rayon = 1;
	protected double hauteur = 3;
	
	public Cylindre() {
		super();
		init();
	}
	
	public Cylindre(double m_rayon, double m_hauteur,Color c, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ) throws IOException {
		super(c,scale,posX,posY,posZ,rotX,rotY,rotZ);
		this.rayon = m_rayon;
		this.hauteur = m_hauteur;
		init();
	}

	
	private void init() {
		Point origin = new Point(0,0,0);
		Point up = new Point(0,(this.hauteur/2),0);
		Point bottom = new Point(0,(-this.hauteur/2),0);
		double degree = 0;
		double x,z,theta;
		ArrayList<Point> listPoint = new ArrayList<Point>();
		for (int i = 0; i < 10; i++) {
			theta = 2*Math.PI/360*degree;
			x = origin.getX()+this.rayon*Math.cos(theta);
			z = origin.getY()+this.rayon*Math.sin(theta);
			if (i < 5) {
				Point p = new Point(x,(this.hauteur/2),z);
				listPoint.add(p);
			}else {
				Point p = new Point(x,(-this.hauteur/2),z);
				listPoint.add(p);
			}
			if (i == 4) {
				degree = -27;
			}
			degree += 72;
		}
		
		for (int i = 0; i < 5; i++) {
			Point p0, p1, p2, p3;
			if (i == 4) {
				p0 = listPoint.get(4);
				p1 = listPoint.get(0);
				p2 = listPoint.get(5);
				p3 = listPoint.get(9);
			}else {
				p0 = listPoint.get(i);
				p1 = listPoint.get(i+1);
				p2 = listPoint.get(9-i);
				p3 = listPoint.get(9-i-1);
			}
			Triangle t = new Triangle(p0,up,p1,this.color);
			this.getListTriangle().add(t);
			this.getListVertex().add(p0);
			this.getListVertex().add(up);
			this.getListVertex().add(p1);
			
			Triangle t2 = new Triangle(p2,bottom,p3,this.color);
			this.getListTriangle().add(t2);
			this.getListVertex().add(p2);
			this.getListVertex().add(bottom);
			this.getListVertex().add(p3);
		}
		
		for (int i = 0; i < 5; i++) {
				Point p0, p1, p2, p3;
				if (i == 4) {
					p0 = listPoint.get(4);
					p1 = listPoint.get(0);
					p2 = listPoint.get(9);
					p3 = listPoint.get(5);
				}else {
					p0 = listPoint.get(i);
					p1 = listPoint.get(i+1);
					p2 = listPoint.get(i+5);
					p3 = listPoint.get(i+6);
				}
				Triangle t = new Triangle(p2,p0,p1,this.color);
				this.getListTriangle().add(t);
				this.getListVertex().add(p2);
				this.getListVertex().add(p0);
				this.getListVertex().add(p1);
				
				Triangle t2 = new Triangle(p2,p1,p3,this.color);
				this.getListTriangle().add(t2);
				this.getListVertex().add(p2);
				this.getListVertex().add(p1);
				this.getListVertex().add(p3);
		}
		
		
	}

}
