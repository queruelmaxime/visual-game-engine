package object;

import java.awt.Color;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;

import mathematics.*;
import render.RenderPolygon;

public class Triangle {

	protected Point firstVertex;
	protected Point secondVertex;
	protected Point thirdVertex;
	
	protected Edge firstEdge;
	protected Edge secondEdge;
	protected Edge thirdEdge;
	
	protected Vector3 normal;
	
	protected Color color = Color.RED;
	protected Color colorLight;

	public Triangle(Point m_v1, Point m_v2, Point m_v3, Color c) {
		this.firstVertex = m_v1;
		this.secondVertex = m_v2;
		this.thirdVertex = m_v3;
		Vector3 tmp1 = this.getFirstVertex().toVector3();
		Vector3 tmp2 = this.getSecondVertex().toVector3();
		Vector3 tmp3 = this.getThirdVertex().toVector3();
		this.firstEdge = new Edge(tmp3.substract(tmp2));
		this.secondEdge = new Edge(tmp1.substract(tmp3));
		this.thirdEdge = new Edge(tmp2.substract(tmp1));
		this.color = c;
		this.colorLight = c;
		normal();
	}
	
	public Triangle(Point m_v1, Point m_v2, Point m_v3) {
		this.firstVertex = m_v1;
		this.secondVertex = m_v2;
		this.thirdVertex = m_v3;
		Vector3 tmp1 = this.getFirstVertex().toVector3();
		Vector3 tmp2 = this.getSecondVertex().toVector3();
		Vector3 tmp3 = this.getThirdVertex().toVector3();
		this.firstEdge = new Edge(tmp3.substract(tmp2));
		this.secondEdge = new Edge(tmp1.substract(tmp3));
		this.thirdEdge = new Edge(tmp2.substract(tmp1));
		normal();
	}

	public Triangle() {
		// TODO Auto-generated constructor stub
	}

	public static double perimeter(Triangle t) {
		return t.firstEdge.getLength() + t.secondEdge.getLength() + t.thirdEdge.getLength();
	}
	
	public static double area(Triangle t) {
		return (t.getFirstEdge().getVector3().crossProduct(t.getSecondEdge().getVector3()).magnitude())/2;
	}
	
	public static Point gravity(Triangle t) {
		Vector3 p1 = t.getFirstVertex().toVector3();
		Vector3 p2 = t.getSecondVertex().toVector3();
		Vector3 p3 = t.getThirdVertex().toVector3();
		return Vector3.vector3ToPoint((p1.add(p2).add(p3)).scalarDivision(3));
	}
	
	public void normal() {
		Vector3 vec1 = secondVertex.toVector3().add(firstVertex.toVector3().scalarMultiply(-1));
		Vector3 vec2 = thirdVertex.toVector3().add(firstVertex.toVector3().scalarMultiply(-1));
		Vector3 normal = vec1.crossProduct(vec2);
		normal.normaliser();
		this.normal = normal;
	}
	
	private static double function(Point a, Point b, Point p) {
		return (a.getY()-b.getY())*p.getX() + (b.getX()-a.getX())*p.getY() + a.getX()*b.getY() - b.getX()*a.getY();
	}
	
	public static ArrayList<Double> barycentrique(Triangle t, Point p, Polygon pol){
		
		double b1 = function(t.getSecondVertex(), t.getThirdVertex(), p)/function(t.getSecondVertex(), t.getThirdVertex(), t.getFirstVertex());
		double b2 = function(t.getThirdVertex(), t.getFirstVertex(), p)/function(t.getThirdVertex(), t.getFirstVertex(), t.getSecondVertex());
		double b3 = function(t.getFirstVertex(), t.getSecondVertex(), p)/function(t.getFirstVertex(), t.getSecondVertex(), t.getThirdVertex());
		
		ArrayList<Double> listBary = new ArrayList<Double>();
		listBary.add(b1);
		listBary.add(b2);
		listBary.add(b3);
		
		return listBary;
	}
	
	public static boolean isIn(Triangle t, Point p, Polygon pol) {
		ArrayList<Double> listBary = barycentrique(t,p, pol);	
		for (int i = 0; i < listBary.size(); i++) {
			if (listBary.get(i) > 1 || listBary.get(i) < 0) {
				return false;
			}
		}
		return true;
	}
	
	public static ArrayList<Point> makeList(Triangle t){
		ArrayList<Point> list = new ArrayList<Point>();
		list.add(t.firstVertex);
		list.add(t.secondVertex);
		list.add(t.thirdVertex);
		return list;
	}
	
	public static ArrayList<Edge> makeListEdge(Triangle t){
		ArrayList<Edge> list = new ArrayList<Edge>();
		list.add(t.firstEdge);
		list.add(t.secondEdge);
		list.add(t.thirdEdge);
		return list;
	}
	
	public static Point minY(Triangle t) {
    	ArrayList<Point> listPoint = Triangle.makeList(t);
    	
    	Collections.sort(listPoint, RenderPolygon.CompareY);
    	return listPoint.get(0);
	}
	
	public static Point maxY(Triangle t) {
    	ArrayList<Point> listPoint = Triangle.makeList(t);
    	
    	Collections.sort(listPoint, RenderPolygon.CompareY);
    	return listPoint.get(2);
	}
	
	//GETTER AND SETTER

	public Point getFirstVertex() {
		return firstVertex;
	}


	public void setFirstVertex(Point firstVertex) {
		this.firstVertex = firstVertex;
	}


	public Point getSecondVertex() {
		return secondVertex;
	}


	public void setSecondVertex(Point secondVertex) {
		this.secondVertex = secondVertex;
	}


	public Point getThirdVertex() {
		return thirdVertex;
	}


	public void setThirdVertex(Point thirdVertex) {
		this.thirdVertex = thirdVertex;
	}


	public Edge getFirstEdge() {
		return firstEdge;
	}


	public void setFirstEdge(Edge firstEdge) {
		this.firstEdge = firstEdge;
	}


	public Edge getSecondEdge() {
		return secondEdge;
	}


	public void setSecondEdge(Edge secondEdge) {
		this.secondEdge = secondEdge;
	}


	public Edge getThirdEdge() {
		return thirdEdge;
	}


	public void setThirdEdge(Edge thirdEdge) {
		this.thirdEdge = thirdEdge;
	}
	
	public Vector3 getNormal() {
		return normal;
	}


	public void setNormal(Vector3 normal) {
		this.normal = normal;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public void setColor(Color c) {
		this.color = c;
	}
	
	public Color getColorLight() {
		return colorLight;
	}

	public void setColorLight(Color colorLight) {
		this.colorLight = colorLight;
	}
	
	//COMPARATOR
	public static Comparator<Point> CompareY = new Comparator<Point>() {

		@Override
		public int compare(Point arg0, Point arg1) {
			return (int) (arg0.getY() - arg1.getY());
		}

	};
	
}
