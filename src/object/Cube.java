package object;

import java.awt.Color;
import java.io.IOException;

import mathematics.Point;

public class Cube extends Polygon {
	
	//CONSTRUCTOR ----------------------------------------
	
	public Cube() {
		super();
		add(Color.RED);
	}
	
	public Cube(Color c, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ) throws IOException {
		super(c,scale,posX,posY,posZ,rotX,rotY,rotZ);
		add(c);
	}
	
	//METHOD ---------------------------------------------
	
	private void add(Color color) {
		//DEFINE ALL 8 POINTS
		Point p1 = new Point(-0.5,-0.5,0.5);
		Point p2 = new Point(0.5,-0.5,0.5);
		Point p3 = new Point(-0.5,0.5,0.5);
		Point p4 = new Point(0.5,0.5,0.5);
		Point p5 = new Point(-0.5,0.5,-0.5);
		Point p6 = new Point(0.5,0.5,-0.5);
		Point p7 = new Point(-0.5,-0.5,-0.5);
		Point p8 = new Point(0.5,-0.5,-0.5);
		
		//CREATE TRIANGLE WITH POINT AND ADD TO POLYGON
		Triangle t1 = new Triangle(p1,p2,p3,color);
		this.addTriangle(t1);
		
		Triangle t2 = new Triangle(p3,p2,p4,color);
		this.addTriangle(t2);
		
		Triangle t3 = new Triangle(p3,p4,p5,color);
		this.addTriangle(t3);
		
		Triangle t4 = new Triangle(p5,p4,p6,color);
		this.addTriangle(t4);
		
		Triangle t5 = new Triangle(p5,p6,p7,color);
		this.addTriangle(t5);
		
		Triangle t6 = new Triangle(p7,p6,p8,color);
		this.addTriangle(t6);
		
		Triangle t7 = new Triangle(p7,p8,p1,color);
		this.addTriangle(t7);
		
		Triangle t8 = new Triangle(p1,p8,p2,color);
		this.addTriangle(t8);
		
		Triangle t9 = new Triangle(p2,p8,p4,color);
		this.addTriangle(t9);
		
		Triangle t10 = new Triangle(p4,p8,p6,color);
		this.addTriangle(t10);
		
		Triangle t11 = new Triangle(p7,p1,p5,color);
		this.addTriangle(t11);
		
		Triangle t12 = new Triangle(p5,p1,p3,color);
		this.addTriangle(t12);
	}

}
