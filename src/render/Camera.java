package render;

import mathematics.*;


public class Camera {
	
	//VARIABLES ------------------------------------------
	
	protected Point winPos;
	protected Point winRes;
	protected Point winCenter;
	protected Point worldPos;
	protected Euler rotation;//rotation de la cam�ra 
	
	protected double zoom;
	protected static double fov = (Math.PI*90)/180;
	
	protected static double far = 100;
	protected static double near = 0.3;
	
	protected static double top = Window.HEIGHT;
	protected static double bottom = -Window.HEIGHT;
	protected static double left = -Window.WIDTH;
	protected static double right = Window.WIDTH;
	
	protected static double aspect = Window.WIDTH/Window.HEIGHT	;
	
	protected double speed;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Camera() {
		this.winPos = new Point(0,0,0);
		this.winRes = new Point(Window.WIDTH, Window.HEIGHT, 0);
		this.winCenter = new Point(this.winRes.getX()/2,this.winRes.getY()/2,0);
		this.worldPos = new Point(0,0,0);
		this.rotation = new Euler();
		this.speed = 10;
	}
	
	//METHOD ---------------------------------------------
	
	public static final Matrix projectionMatrix() {
		Matrix projection = new Matrix(4);
		projection.changeValue(
				(2/(right-left)),0,0,-((right+left)/(right-left)),
				0,(2/(top-bottom)),0,-((top+bottom)/(top-bottom)),
				0,0,(-2/(far-near)),-((far+near)/(far-near)),
				0,0,0,1
				);
		return projection;
	}
	public static final Matrix viewportMatrix(Camera c) {
		Matrix viewport = new Matrix(4);
		viewport.changeValue(
				((right-left)/2)/2,0,0, c.getWinPos().getX() + (Window.WIDTH/2),
				0,((top-bottom)/2)/2,0,c.getWinPos().getY() + (Window.HEIGHT/2),
				0,0,(far-near)/2,(near+far)/2,
				0,0,0,1
				);
		return viewport;
	}
	
	public static final Matrix viewportMatrixPer(Camera c) {
		Matrix viewport = new Matrix(4);
		viewport.changeValue(
				(Window.WIDTH)/2,0,0, c.getWinPos().getX() + (Window.WIDTH/2),
				0,(Window.HEIGHT)/2,0,c.getWinPos().getY() + (Window.HEIGHT/2),
				0,0,(far-near)/2,(near+far)/2,
				0,0,0,1
				);
		return viewport;
	}
	
	public static Matrix viewMatrix(Camera c) {
		Matrix view = Matrix.eulerToMatrix(c.rotation);
		
		view.changeOneValue(0, 3, c.getWinPos().getX());
		view.changeOneValue(1, 3, c.getWinPos().getY());
		view.changeOneValue(2, 3, c.getWinPos().getZ());
		
//		Vector3 z = viewDirection.scalarMultiply(-1);
//		z = z.scalarDivision(viewDirection.magnitude());
//		
//		Vector3 x = viewDirection.crossProduct(up);
//		x = x.scalarDivision((viewDirection.crossProduct(up)).magnitude());
//		
//		Vector3 y = z.crossProduct(x);
//		view.changeValue(
//				x.getX(),y.getX(),z.getX(),c.winPos.getX(),
//				x.getY(),y.getY(),z.getY(),c.winPos.getY(),
//				x.getZ(),y.getZ(),z.getZ(),c.winPos.getZ(),
//				0,0,0,1
//				);
		return view.inverse();
	}


	public static final Matrix projectionMatrixPerspective() {
		Matrix projection = new Matrix(4);
		double d = 1/(Math.tan(fov/2));
		projection.changeValue(
				d/aspect,0,0,0,
				0,d/aspect,0,0,
				0,0,(near+far)/(near-far),(2*near*far)/(near-far),
				0,0,-1,0
				);
		return projection;
	}
	
	public static final Matrix leftHand() {
		Matrix left = new Matrix(4);
		left.changeValue(
				-1,0,0,0,
				0,-1,0,0,
				0,0,1,0,
				0,0,0,1
				);
		return left;
	}
	
	//GETTER AND SETTER ----------------------------------

	public Point getWinPos() {
		return winPos;
	}

	public void setWinPos(Point winPos) {
		this.winPos = winPos;
	}

	public Point getWinRes() {
		return winRes;
	}

	public Point getWorldPos() {
		return worldPos;
	}

	public void setWorldPos(Point worldPos) {
		this.worldPos = worldPos;
	}

	public void setWinRes(Point winRes) {
		this.winRes = winRes;
	}

	public Point getWinCenter() {
		return winCenter;
	}

	public void setWinCenter(Point winCenter) {
		this.winCenter = winCenter;
	}

	public double getZoom() {
		return zoom;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public double getFov() {
		return fov;
	}
	
	public double getNear() {
		return near;
	}
	
	public double getFar() {
		return far;
	}

}
