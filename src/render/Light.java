package render;


import mathematics.*;

public class Light {
	
	protected Vector3 diffuse;
	protected Vector3 direction;

	public Light(Vector3 m_diffuse, Vector3 m_direction) {
		diffuse = m_diffuse;
		direction = m_direction.normaliser();
	}
	
	
}
