package render;

import java.awt.Color;


import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Comparator;

import mathematics.*;
import object.*;

public abstract class RenderPolygon {
	
	static double a1;
	static double a2;
	static double step = 1;
	static double beta = 0.0002;
	static double theta = 0;
	static ArrayList<Point> points = new ArrayList<Point>();
	static Triangle tmp;
	
	public static ArrayList<Point> changePoint(Camera c, Polygon p, boolean perspective){
		ArrayList<Point> pointList = new ArrayList<Point>();

		for (int i = 0; i < p.getListVertex().size(); i++) {
			
			Vector4 pos = Vector3.toVector4(p.getListVertex().get(i).toVector3());
			Matrix posM = Vector4.vector4ToMatrix(pos);
			
			Matrix translate = Matrix.translate(p.getPosition().getX(), p.getPosition().getY(), p.getPosition().getZ());
			Matrix scale = Matrix.scale(p.getScale()[0], p.getScale()[1], p.getScale()[2]);
			
			Matrix rotation = p.setMatrix();
			p.setRotationMaxtrix(rotation);
			
			if (p.SLERP == true) {
				rotation = p.getRotationMaxtrix();
				Quaternion q0 = Quaternion.matrixToQuaternion(rotation);
				Quaternion q1 = Quaternion.eulerToQuaternion(new Euler());
				if (theta < 1) {
					rotation = Matrix.quaternionToMatrix(Quaternion.SLERP(q0, q1, theta));
					p.setRotationMaxtrix(rotation);
					theta += 0.0002;
				}else {
					p.SLERP = false;
					p.setRotateX(0);
					p.setRotateY(0);
					p.setRotateZ(0);
					theta = 0;
				}
			}
			
			if (p.LERP == true) {
				if (beta < 1) {
					Euler rotationEuler = Euler.matrixToEuler(rotation);
					rotation = Matrix.eulerToMatrix(Euler.LERP(rotationEuler, new Euler(), beta));
					p.setRotationMaxtrix(rotation);
					beta += 0.0002;
				}else {
					p.setRotateX(0);
					p.setRotateY(0);
					p.setRotateZ(0);
					p.LERP = false;
					beta = 0;
				}
			}
			
			Matrix transform = translate.multiply(rotation.multiply(scale.multiply(posM)));
			
			Matrix render = null;
			
			if (!perspective) {
				render = Camera.viewMatrix(c).multiply(Camera.projectionMatrix());
				render = render.multiply(transform);
				render = Camera.viewportMatrix(c).multiply(render);
				
				Point newPoint = new Point(render.getComponent().get(0).get(0), render.getComponent().get(1).get(0), render.getComponent().get(2).get(0));
				
				pointList.add(newPoint);
			}else {
				
				points.add(new Point(
						transform.getComponent().get(0).get(0), 
						transform.getComponent().get(1).get(0), 
						transform.getComponent().get(2).get(0)
						));
				
				if (i%3 == 2 && i != 0 && Window.isLighting) {
					
					Triangle t = p.recupTriangle(i);
					
					tmp = new Triangle(points.get(0),points.get(1),points.get(2));
					Light light = Window.light;
					double tmpV = tmp.getNormal().dotProduct(light.direction);
					
					double red = checkColor((light.diffuse.getX() * t.getColor().getRed() * tmpV)/256f);
					double green =  checkColor((light.diffuse.getY() * t.getColor().getGreen() * tmpV)/256f);
					double blue = checkColor((light.diffuse.getZ() * t.getColor().getBlue() * tmpV)/256f);
					Color finalColor = new Color((int) (red*255), (int) (green*255), (int) (blue*255));
					t.setColorLight(finalColor);
					points.clear();
				}
				
				if (!Window.isLighting) {
					Triangle t = p.recupTriangle(i);
					t.setColorLight(t.getColor());
				}
				
				
				render = Camera.viewMatrix(c).multiply(transform);
				render = Camera.projectionMatrixPerspective().multiply(render);
				if (render.getComponent().get(3).get(0) < 0) {
					render.scalarMultiply(1/render.getComponent().get(3).get(0));	
					render = Camera.viewportMatrixPer(c).multiply(render);
					
					Point newPoint = new Point(render.getComponent().get(0).get(0), render.getComponent().get(1).get(0), render.getComponent().get(2).get(0));
					
					pointList.add(newPoint);
				}

			}
			

		}
		return pointList;
	}
	
		public static void renderByVertex(Graphics g, Camera c, Polygon p, boolean perspective) {
		g.setColor(Color.BLACK);
		ArrayList<Point> pointList = RenderPolygon.changePoint(c,p,perspective);
		int count = 0;
		
		Boolean display = true;
		
		if (p.getDistance() >= c.getFar()*10) {
			display = false;
		}
		
		if(display) {
			for (int j = 0; j < pointList.size()-1; j++) {
	
				if ((j%3) == 0) {
					
					p.getListTriangle().get(count).setFirstVertex(pointList.get(j));
					p.getListTriangle().get(count).setSecondVertex(pointList.get(j+1));
					if (j+1 ==  pointList.size()-1) {
						p.getListTriangle().get(count).setThirdVertex(pointList.get(0));
					}else {
						p.getListTriangle().get(count).setThirdVertex(pointList.get(j+2));
					}

					p.getListTriangle().get(count).normal();
					
					if (p.getListTriangle().get(count).getNormal().getZ() < 0) {
						
						if (!Window.wireframe) {
							if (Window.isColor) {
								fillPolygon(g, c, p.getListTriangle().get(count),p);
							}else {
								g.setColor(p.getListTriangle().get(count).getColorLight());
								if (Window.isClick) {
									if (Triangle.isIn(p.getListTriangle().get(count), Window.mouse, p)) {
										g.setColor(Color.GREEN);
										p.getListTriangle().get(count).setColor(Color.GREEN);
										p.getListTriangle().get(count).setColorLight(Color.GREEN);
										Window.isClick = false;
									}
								}
								int[] xPoints = {
										(int) p.getListTriangle().get(count).getFirstVertex().getX(),
										(int) p.getListTriangle().get(count).getSecondVertex().getX(),
										(int) p.getListTriangle().get(count).getThirdVertex().getX()};
								int[] yPoints = {
										(int) p.getListTriangle().get(count).getFirstVertex().getY(),
										(int) p.getListTriangle().get(count).getSecondVertex().getY(),
										(int) p.getListTriangle().get(count).getThirdVertex().getY()};
								g.fillPolygon(xPoints, yPoints, 3);
								g.setColor(Color.BLACK);
							}
						}
					}
					count++;
				}
			}
		}
		count = 0;
		if (display) {
			for (int i = 0; i < pointList.size()-1; i++) {
				if ((i%3) == 0) {
					
					if (p.getListTriangle().get(count).getNormal().getZ() < 0) {
						
						double x0 = p.getListTriangle().get(count).getFirstVertex().getX();
						double y0 = p.getListTriangle().get(count).getFirstVertex().getY();

						double x1 = p.getListTriangle().get(count).getSecondVertex().getX();
						double y1 = p.getListTriangle().get(count).getSecondVertex().getY();
						
						double x2 = p.getListTriangle().get(count).getThirdVertex().getX();
						double y2 = p.getListTriangle().get(count).getThirdVertex().getY();

						drawLine(g, (int) x0, (int) y0, (int) x1, (int) y1);
						drawLine(g, (int) x1, (int) y1, (int) x2, (int) y2);
						drawLine(g, (int) x2, (int) y2, (int) x0, (int) y0);
						
					}
					count++;
				}
			}
		}
	}
	
    public static void drawLine(Graphics g, int x1, int y1, int x2, int y2) {
        int x, y;
        int dx, dy;
        int incx, incy;
        int balance;

        if (x2 >= x1) {
            dx = x2 - x1;
            incx = 1;
        }else {
            dx = x1 - x2;
            incx = -1;
        }

        if (y2 >= y1) {
            dy = y2 - y1;
            incy = 1;
        }else {
            dy = y1 - y2;
            incy = -1;
        }

        x = x1;
        y = y1;

        if (dx >= dy) {
            dy <<= 1;
            balance = dy - dx;
            dx <<= 1;
            while (x != x2) {
            	g.drawLine(x,y,x,y);
                if (balance >= 0) {
                    y += incy;
                    balance -= dx;
                }
                balance += dy;
                x += incx;
            }
            g.drawLine(x,y,x,y);
        }else {
            dx <<= 1;
            balance = dx - dy;
            dy <<= 1;
            while (y != y2) {
            	g.drawLine(x,y,x,y);
                if (balance >= 0) {
                    x += incx;
                    balance -= dy;
                }
                balance += dx;
                y += incy;
            }
            g.drawLine(x,y,x,y);
        }
    }
    
    
    private static void drawPoint(Graphics g, Camera c, double x, double y) {
		int x0 = (int) (x);
    	int y0 = (int) (y);
    	g.drawLine(x0,y0,x0,y0);
    }
    
    private static void seedFillLeft(Graphics g, Triangle t, Camera c, Polygon pol, Point seed, double x) {
    	if (Triangle.isIn(t, seed, pol)) {
    		drawPoint(g,c,seed.getX(),seed.getY());
    		seed = new Point(seed.getX()+x,seed.getY(),seed.getZ());
    		seedFillLeft(g,t,c,pol,seed,x);
    		a1++;
    	}   	
    }
    
    private static void seedFillRight(Graphics g, Triangle t, Camera c, Polygon pol, Point seed, double x) {
    	if (Triangle.isIn(t, seed, pol)) {
    		drawPoint(g,c,seed.getX(),seed.getY());
    		seed = new Point(seed.getX()+x,seed.getY(),seed.getZ());
    		seedFillRight(g,t,c,pol,seed,x);
    		a2++;
    	}   	
    }
    
    private static void seedFillY(Graphics g, Triangle t, Camera c, Polygon pol, Point seed, double x) {
    	if (Triangle.isIn(t, seed, pol)) {
    		drawPoint(g,c,seed.getX(),seed.getY());
    		a1 = 0;
    		a2 = 0;
    		seedFillLeft(g,t,c,pol,seed,x);
    		seedFillRight(g,t,c,pol,seed,-x);
    		if (x < 0) {
    			seed = new Point((seed.getX()-a1+a2),seed.getY()+x,seed.getZ());
			}else {
				seed = new Point((seed.getX()+a1-a2),seed.getY()+x,seed.getZ());
			}
    		seedFillY(g,t,c,pol,seed,x);
    	}   	
    }
    
	public static void fillPolygon(Graphics g, Camera c, Triangle t, Polygon pol) {
		
		g.setColor(t.getColorLight());
		
		if (Window.isClick) {
			if (Triangle.isIn(t, Window.mouse, pol)) {
				g.setColor(Color.GREEN);
				t.setColor(Color.GREEN);
				Window.isClick = false;
			}
		}
    	
    	Point seed = Triangle.gravity(t);
    	
    	seedFillY(g,t,c,pol,seed,step);   
    	seedFillY(g,t,c,pol,seed,-step); 

    	g.setColor(Color.BLACK);
    	
    }
	
	public static double checkColor(double color) {
		if (color > 1) {
			color = 1;
		}
		if (color < 0) {
			color = 0;
		}
		return color;
	}
	
	public static Comparator<Point> CompareX = new Comparator<Point>() {

		@Override
		public int compare(Point arg0, Point arg1) {
			return (int) (arg0.getX() - arg1.getX());
		}

	};
	
	public static Comparator<Point> CompareY = new Comparator<Point>() {

		@Override
		public int compare(Point arg0, Point arg1) {
			return (int) (arg0.getY() - arg1.getY());
		}

	};

}
