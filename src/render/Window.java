package render;

import java.awt.Canvas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.*;

import mathematics.Euler;
import mathematics.Matrix;
import mathematics.Point;
import mathematics.Quaternion;
import mathematics.Vector3;
import object.Cube;
import object.Cylindre;
import object.Polygon;
import object.Sphere;
import object.Tetrahedron;

public class Window extends Canvas implements Runnable {
	
	//VARIABLES -----------------------------------------------------
	
	protected Thread thread;
	protected JFrame frame;
	protected static String title = "3D Engine";
	public static final int WIDTH = 1024;
	public static final int HEIGHT = 1024;
	protected static boolean running = false;
	protected int comp = 0;
	protected BufferedImage cache = new BufferedImage(WIDTH,  HEIGHT, BufferedImage.TYPE_INT_RGB);
	protected ArrayList<Polygon> models = new ArrayList<Polygon>();
	public Camera camera;
	protected boolean perspective = true;
	protected Vector3 droite = new Vector3();
	public static Boolean wireframe = false;
	public static Boolean isColor = false;
	public static Point mouse;
	public static boolean isClick = false;
	public static Light light = new Light(new Vector3(1,1,1), new Vector3(-0.45,-0.45,-0.3));
	public static Boolean isLighting;
	private int scene = 0;
	public static Point mouseSegment;
	
	//CONSTRUCTOR ---------------------------------------------------
	
	public Window() {
		this.frame = new JFrame();
		
		Dimension size = new Dimension(WIDTH, HEIGHT);
		this.setPreferredSize(size);
	}
	
	//METHOD --------------------------------------------------------

	//START METHOD
	public synchronized void start(){
		running = true;
		this.thread = new Thread(this, "Display");
		this.thread.start();
		isLighting = false;
		
		this.camera = new Camera();
		Window.isClick = false;
	}
	
	//STOP METHOD
	public synchronized void stop(){
		running = false;
		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void render() {
		
		Graphics2D g = (Graphics2D) cache.getGraphics();
	
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		//RENDER ALL OBJECTS
		Collections.sort(models, Window.CompareZ);
		
		Matrix tmpM = new Matrix(4);
		
		for (int i = 0; i < models.size(); i++) {
				RenderPolygon.renderByVertex(g, this.camera, models.get(i), perspective);
				if (i == 0) {
					tmpM = models.get(i).getRotationMaxtrix();
				}
		}

		
		if (scene == 2) {
			g.setColor(Color.BLACK);
			g.drawString("Rotation : ", 390, 20);
			
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(0).get(0)*100))/100, 460, 20);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(0).get(1)*100))/100, 500, 20);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(0).get(2)*100))/100, 540, 20);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(1).get(0)*100))/100, 460, 40);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(1).get(1)*100))/100, 500, 40);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(1).get(2)*100))/100, 540, 40);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(2).get(0)*100))/100, 460, 60);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(2).get(1)*100))/100, 500, 60);
			g.drawString(""+(double) ((int)(tmpM.getComponent().get(2).get(2)*100))/100, 540, 60);
			
			g.drawString("Euler : ", 600, 20);
			Euler e = Euler.matrixToEuler(tmpM);
			g.drawString("Yaw : " + (double) ((int)(e.getYaw()*1000))/1000, 650, 20);
			g.drawString("Pitch : " + (double) ((int)(e.getPitch()*1000))/1000, 650, 40);
			g.drawString("Roll : " + (double) ((int)(e.getRoll()*1000))/1000, 650, 60);
			
			g.drawString("Quaternion : ", 750, 20);
			Quaternion q = Quaternion.matrixToQuaternion(tmpM);
			g.drawString("W : " + (double) ((int)(q.getW()*1000))/1000, 840, 20);
			g.drawString("X : " + (double) ((int)(q.getX()*1000))/1000, 840, 40);
			g.drawString("Y : " + (double) ((int)(q.getY()*1000))/1000, 840, 60);
			g.drawString("Z : " + (double) ((int)(q.getZ()*1000))/1000, 840, 80);			
		}

		
		//AXIS
		Matrix mAxis = Matrix.eulerToMatrix(camera.rotation);
		int x1 = (int) (mAxis.getComponent().get(0).get(0)*100.0);
		int y1 = (int) (mAxis.getComponent().get(0).get(1)*100.0);
		int x2 = (int) (mAxis.getComponent().get(1).get(0)*100.0);
		int y2 = (int) (mAxis.getComponent().get(1).get(1)*100.0);
		int x3 = (int) (mAxis.getComponent().get(2).get(0)*100.0);
		int y3 = (int) (mAxis.getComponent().get(2).get(1)*100.0);
		g.setColor(Color.RED);
		g.drawLine(x1+100, y1+100, 100, 100);
		g.setColor(Color.BLUE);
		g.drawLine(-x2+100, -y2+100, 100, 100);
		g.setColor(Color.GREEN);
		g.drawLine(-x3+100, -y3+100, 100, 100);

		
		g.dispose();
		
		getGraphics().drawImage(cache, 0, 0, null);
		getGraphics().dispose();
	}
	
	private void update() throws IOException {
	}
	
	@Override
	public void run() {
		this.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent arg0) {try {
				keyChoose(arg0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}

			@Override
			public void keyReleased(KeyEvent arg0) {}

			@Override
			public void keyTyped(KeyEvent arg0) {}
			
		});
		
		this.addMouseListener(new MouseAdapter() {
		    @Override 
		    public void mousePressed(MouseEvent e) {
		    	mouse = new Point(e.getX(), e.getY(),0);
		    	isClick = true;
		    }
		  });
		
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		double fps = 60;
		final double ns = 1000000000.0 / fps;
		double delta = 0;
		int frames = 0;
	
		frame.setBackground(Color.BLACK);
		
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1) {
				try {
					update();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				delta--;
				render();
				frames++;
			}
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				frame.setTitle(title + " | " + frames + " fps");
				frames = 0;
			}
		}
	}
	
	public void keyChoose(KeyEvent event) throws IOException {
		
		if (event.getKeyCode() == KeyEvent.VK_DOWN) {
			this.camera.getWinPos().setY(this.camera.getWinPos().getY()-this.camera.speed);
	    }
	    if (event.getKeyCode() == KeyEvent.VK_UP) {
			this.camera.getWinPos().setY(this.camera.getWinPos().getY()+this.camera.speed);
	    }
	    if (event.getKeyCode() == KeyEvent.VK_LEFT) {
			this.camera.getWinPos().setX(this.camera.getWinPos().getX()+this.camera.speed);
	    }
	    if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
			this.camera.getWinPos().setX(this.camera.getWinPos().getX()-this.camera.speed);
	    }	    
	    if (event.getKeyCode() == KeyEvent.VK_S) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setDistance(models.get(i).getDistance()+this.camera.speed);
			}
			this.camera.getWinPos().setZ(this.camera.getWinPos().getZ()-this.camera.speed);
	    }
	    if (event.getKeyCode() == KeyEvent.VK_Z) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setDistance(models.get(i).getDistance()-this.camera.speed);
			}
			this.camera.getWinPos().setZ(this.camera.getWinPos().getZ()+this.camera.speed);
	    }
	    if (event.getKeyCode() == KeyEvent.VK_SPACE) {
	    	this.camera.setWinPos(new Point());
	    }
	    if(event.getKeyCode() == KeyEvent.VK_4) {
	    	perspective = false;
	    	scene = 4;
	    	reset();
	    	Polygon p = new Cube(Color.BLUE,50,200,-200,100,0,0,0);
			Polygon p2 = new Sphere(1,Color.GREEN,50,-200,-200,100,0,27,27);
			Polygon p3 = new Cylindre(1,3,Color.ORANGE,75,200,200,100,0,27,27);
			Polygon p4 = new Tetrahedron(Color.MAGENTA,50,-200,200,100,0,27,27);
			this.getModels().add(p);
			this.getModels().add(p2);
			this.getModels().add(p3);
			this.getModels().add(p4);
	    }
	    if(event.getKeyCode() == KeyEvent.VK_1) {
	    	perspective = false;
	    	scene = 1;
	    	reset();
	    	Polygon p = new Cube(Color.BLUE,50,0,0,100,0,0,0);
	    	this.getModels().add(p);
	    }
	    if(event.getKeyCode() == KeyEvent.VK_2) {
	    	perspective = true;
	    	scene = 2;
	    	reset();
	    	Polygon p = new Cube(Color.YELLOW,50,0,0,100,0,0,0);
	    	this.getModels().add(p);
	    }
	    if(event.getKeyCode() == KeyEvent.VK_3) {
	    	perspective = true;
	    	scene = 3;
	    	reset();
	    	Polygon p = new Cube(Color.RED,50,0,0,100,0,0,0);
	    	Polygon p2 = new Cube(Color.ORANGE,50,0,0,-100,0,0,0);
	    	this.getModels().add(p);
	    	this.getModels().add(p2);
	    }
	    if(event.getKeyCode() == KeyEvent.VK_5) {
	    	perspective = true;
	    	scene = 5;
	    	reset();
	    	Polygon p = new Polygon("Poke.obj",Color.RED,50,0,0,400,0,220,0);
	    	this.getModels().add(p);
	    }
	    if(event.getKeyCode() == KeyEvent.VK_6) {
	    	perspective = false;
	    	scene = 6;
	    	reset();
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD0) {
	    	reset();
	    	camera.rotation = new Euler();
	    }
	    if(event.getKeyCode() == KeyEvent.VK_0) {
	    	isColor = !isColor;
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD1) {
			wireframe = !wireframe;
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD2) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).LERP = true;	
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD3) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).SLERP = true;
				models.get(i).setRotationMaxtrix(models.get(i).setMatrix());
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD4) {
			camera.rotation.setPitch(camera.rotation.getPitch()+((Math.PI*1)/180));
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD5) {
	    	camera.rotation.setRoll(camera.rotation.getRoll()+((Math.PI*1)/180));
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD6) {
	    	camera.rotation.setYaw(camera.rotation.getYaw()+((Math.PI*1)/180));
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD7) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setRotateY(models.get(i).getRotateY()+1);
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD8) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setRotateX(models.get(i).getRotateX()+1);
			}
		}
	    if(event.getKeyCode() == KeyEvent.VK_NUMPAD9) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setRotateZ(models.get(i).getRotateZ()+1);
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_M) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setScale(increaseList(models.get(i).getScale(), -1));
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_P) {
			for (int i = 0; i < models.size(); i++) {
				models.get(i).setScale(increaseList(models.get(i).getScale(), 1));
			}
	    }
	    if(event.getKeyCode() == KeyEvent.VK_L) {
	    	isLighting = !isLighting;
	    }
	}
	
	private void speedCamera() {
		camera.speed = 10;
		if (!perspective) {
			camera.speed = 0.01;
		}
	}
	
	public static Comparator<Polygon> CompareZ = new Comparator<Polygon>() {

		@Override
		public int compare(Polygon arg0, Polygon arg1) {
			return (int) (arg0.getDistance() - arg1.getDistance()); 
		}
	};
	
	public void reset() {
		this.models.clear();
		speedCamera();
		Window.isClick = false;
	}
	
	public double[] increaseList(double[] lst, int step) {
		for (int i = 0; i < lst.length; i++) {
			lst[i] = lst[i] + step;
		}
		return lst;
	}
	
	//GETTER AND SETTER  --------------------------------------------------------
	
	public Thread getThread() {
		return thread;
	}

	public JFrame getFrame() {
		return frame;
	}
	
	public static String getTitle() {
		return title;
	}
	
	public static boolean isRunning() {
		return running;
	}
	
	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}
	
	public ArrayList<Polygon> getModels(){
		return this.models; 
	}
	
	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public static void setTitle(String title) {
		Window.title = title;
	}

	public static void setRunning(boolean running) {
		Window.running = running;
	}
}

