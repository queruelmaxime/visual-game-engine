package render;

import object.*;

import java.awt.Color;
import java.io.*;
import java.util.*;

import mathematics.*;

public class FileReader {
	
	public void readFile(Polygon obj, String file, Color color) throws java.io.IOException{
		
		InputStream fichier = this.getClass().getClassLoader().getResourceAsStream(file);
		Scanner lecteur ;
		lecteur = new Scanner(fichier);
		
		boolean boolPoint = false;
		int incPoint = 0;
		String[] pointCoords = new String[3];
		ArrayList<Point> listPoint = new ArrayList<Point>();
		
		//POLYGON RECUP VARIABLES
		boolean boolTriangle = false;
		int incTriangle = 0;
		Integer[] trianglePointNumber = new Integer[4];
		ArrayList<Triangle> listTriangle = new ArrayList<Triangle>();
		String strPoint = "";
		
		while (lecteur.hasNext()) {
			
			String tmp = lecteur.next();
			
			//RECUP POINT
			
			if (incPoint == 3) {
				boolPoint = false;
				incPoint = 0;
				Point point = new Point(Double.parseDouble(pointCoords[0]), Double.parseDouble(pointCoords[1]), Double.parseDouble(pointCoords[2]));
				listPoint.add(point);
			}
			
			if (boolPoint) {
				pointCoords[incPoint] = tmp;
				incPoint++;
			}
			
			if (tmp.equals("v")) {
				boolPoint = true;
			}
			
			if (boolTriangle) {
				for (int i = 0; i < tmp.length(); i++) {
					if (tmp.charAt(i) != '/') {
						strPoint += tmp.charAt(i);
					}else {
						break;
					}
				}
				
				trianglePointNumber[incTriangle] = Integer.parseInt(strPoint);
				strPoint = "";
				incTriangle++;
			}
			
			if (tmp.equals("f")) {
				boolTriangle = true;
			}
			
			if (incTriangle == 3) {
				boolTriangle = false;
				incTriangle = 0;
				Triangle poly = new Triangle(
						listPoint.get(trianglePointNumber[0]-1),
						listPoint.get(trianglePointNumber[1]-1),
						listPoint.get(trianglePointNumber[2]-1),
						color
						);
				listTriangle.add(poly);
			}
		}
		
		for (int i = 0; i < listTriangle.size(); i++) {
			obj.addTriangle(listTriangle.get(i));
		}
		
		lecteur.close();
	}
}

