package main;

import javax.swing.JFrame;

import render.Window;

public class Main {
	
	public static void main(String[] args){
		
		//INIT WINDOW
		Window window = new Window();
		window.getFrame().setTitle(Window.getTitle());
		window.getFrame().add(window);
		window.getFrame().pack();
		window.getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.getFrame().setLocationRelativeTo(null);
		window.getFrame().setResizable(false);
		window.getFrame().setVisible(true);

		//START WINDOW
		window.start();
		
	}
}
