package mathematics;

public class Edge extends Vector3 {
	
	protected double length;
	
	public Edge() {
		super();
		this.length = 0;
	}
	
	public Edge(Vector3 v) {
		super(v.x,v.y,v.z);
		this.length = v.magnitude();
	}
	
	public Edge(Point p) {
		super(p.getX(), p.getY(), p.getZ());
		this.length = this.magnitude();
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	
	

}
