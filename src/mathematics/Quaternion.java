package mathematics;

public class Quaternion {
	
	//VARIABLES ------------------------------------------
	
	protected double x;
	protected double y;
	protected double z;
	protected double w;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Quaternion() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.w = 0;
	}
	
	public Quaternion(double m_x, double m_y, double m_z) {
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
		this.w = 1;
	}
	
	public Quaternion(double m_x, double m_y, double m_z, double m_w) {
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
		this.w = m_w;
	}
	
	//METHOD ---------------------------------------------
	
	public String toString() {
		return "Quaternion : \n" + 
				"	q.x = " + this.x + "\n" +
				"	q.i = " + this.y + "\n" +
				"	q.z = " + this.z + "\n" +
				"	q.w = " + this.w;
	}
	
	public static Quaternion identity() {
		return new Quaternion(0,0,0,1);
	}
	
	public double magnitude() {
		return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z + this.w*this.w);
	}
	
	public static Quaternion conjugate(Quaternion q) {
		return new Quaternion(-q.x, -q.y, -q.z, q.w);
	}
	
	public static Quaternion inverse(Quaternion q) {
		double magnitude = q.magnitude();
		Quaternion qC = Quaternion.conjugate(q); 
		return new Quaternion(qC.x/magnitude,qC.y/magnitude,qC.z/magnitude,qC.w/magnitude);
	}
	
	public static Quaternion add(Quaternion q, Quaternion r) {
		return new Quaternion(q.x+r.x,q.y+r.y,q.z+r.z,q.w+r.w);
	}
	
	public static Quaternion productHamilton(Quaternion q, Quaternion r) {
		return new Quaternion(
				q.y*r.z - q.z*r.y + r.w*q.x + q.w*r.x,
				q.z*r.x - q.x*r.z + r.w*q.y + q.w*r.y,
				q.x*r.y - q.y*r.x + r.w*q.z + q.w*r.z,
				q.w*r.w - q.x*r.x - q.y*r.y - q.z*r.z
				);
	}
	
	public static Quaternion diff(Quaternion q, Quaternion r) {
		return Quaternion.productHamilton(r, Quaternion.inverse(q));
	}
	
	public static Quaternion scalarMultiply(Quaternion q, double l) {
		return new Quaternion(q.z*l, q.y*l, q.z*l, q.w*l);
	}
	
	public static double scalarProduct(Quaternion q, Quaternion r) {
		return q.w*r.w + q.x*r.x + q.y*r.y + q.w*r.w;
	}
	
	public static double dotProduct(Quaternion q0, Quaternion q1) {
		return q0.x * q1.x + q0.y * q1.y + q0.z * q1.z + q0.w * q1.w;
	}
	
	public static Quaternion normaliser(Quaternion q) {
		return new Quaternion(q.x/(Math.sqrt(q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w)),
				q.y/(Math.sqrt(q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w)),
				q.z/(Math.sqrt(q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w)), 
				q.w/(Math.sqrt(q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w)));
	}
	
	public static Quaternion SLERP(Quaternion q0, Quaternion q1, double t) {
			double cosW = Quaternion.dotProduct(q0, q1);
			double w = Math.acos(cosW);
			double invSinTheta = 1/Math.sin(w);
			double k0 = (Math.sin(1-t)*w)*invSinTheta;
			double k1 = Math.sin(t*w)*invSinTheta;
			return Quaternion.add(Quaternion.scalarMultiply(q0, k0), Quaternion.scalarMultiply(q1, k1));
	}

	public static Quaternion NLERP(Quaternion q0, Quaternion q1, double t) {
		Quaternion newQ0 = Quaternion.scalarMultiply(q0, (1-t));
		Quaternion newQ1 = Quaternion.scalarMultiply(q1, t);
		Quaternion sum = Quaternion.add(newQ0, newQ1);
		double norm = sum.magnitude();
		return Quaternion.scalarMultiply(sum, (1/norm));
	}
	
	public static Quaternion matrixToQuaternion(Matrix m) {
		
		Quaternion q = new Quaternion();

		double m11 = m.getComponent().get(0).get(0);
		double m12 = m.getComponent().get(0).get(1);
		double m13 = m.getComponent().get(0).get(2);
		double m21 = m.getComponent().get(1).get(0);
		double m22 = m.getComponent().get(1).get(1);
		double m23 = m.getComponent().get(1).get(2);
		double m31 = m.getComponent().get(2).get(0);
		double m32 = m.getComponent().get(2).get(1);
		double m33 = m.getComponent().get(2).get(2);
		
		double fourWSquaredMinus = m11 + m22 + m33;
		double fourXSquaredMinus = m11 - m22 - m33;
		double fourYSquaredMinus = m22 - m11 - m33;
		double fourZSquaredMinus = m33 - m11 - m22;
		
		int biggestIndex = 0;
		double fourBiggestSquaredMinus = fourWSquaredMinus;
		
		if (fourXSquaredMinus > fourBiggestSquaredMinus) {
			fourBiggestSquaredMinus = fourXSquaredMinus;
			biggestIndex = 1;
		}
		if (fourYSquaredMinus > fourBiggestSquaredMinus) {
			fourBiggestSquaredMinus = fourYSquaredMinus;
			biggestIndex = 2;
		}
		if (fourZSquaredMinus > fourBiggestSquaredMinus) {
			fourBiggestSquaredMinus = fourZSquaredMinus;
			biggestIndex = 3;
		}
		
		double biggestVal = Math.sqrt(fourBiggestSquaredMinus + 1) * 0.5;
		double mult = 0.25 / biggestVal;
		
		switch (biggestIndex) {
		case 0:
			q.w = biggestVal;
			q.x = (m23 - m32) * mult;
			q.y = (m31 - m13) * mult;
			q.z = (m12 - m21) * mult;
			break;
		case 1:
			q.x = biggestVal;
			q.w = (m23 - m32) * mult;
			q.y = (m12 - m21) * mult;
			q.z = (m31 - m13) * mult;
			break;
		case 2:
			q.y = biggestVal;
			q.w = (m31 - m13) * mult;
			q.x = (m12 - m21) * mult;
			q.z = (m23 - m32) * mult;
			break;
		case 3:
			q.z = biggestVal;
			q.w = (m12 - m21) * mult;
			q.x = (m31 - m13) * mult;
			q.y = (m23 - m32) * mult;
			break;
		}
		
		return q;
		
	}
	
	public static Quaternion eulerToQuaternion(Euler e) {
		Matrix m = Matrix.eulerToMatrix(e);
		return Quaternion.matrixToQuaternion(m);
	}
	
	//GETTER AND SETTER ----------------------------------
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

}
