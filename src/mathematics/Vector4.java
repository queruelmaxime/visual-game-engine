package mathematics;

public class Vector4 {
	
	//VARIABLES ------------------------------------------
	
	protected double x;
	protected double y;
	protected double z;
	protected double w;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Vector4() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.w = 0;
	}
	
	public Vector4(double m_x, double m_y, double m_z, double m_w) {
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
		this.w = m_w;
	}
	
	//METHOD ---------------------------------------------
	
	public String toString() {
		return "Vector 4 : <" + this.x + "," + this.y + "," + this.z + "," + this.w + ">";
	}
	
	public void changeValue(double m_x, double m_y, double m_z, double m_w) { //CHANGE ALL VALUE
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
		this.w = m_w;
	}
	
	public void negation() {
		this.changeValue(-this.x, -this.y, -this.z, -this.w);
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2) + Math.pow(this.z, 2));
	}
	
	public Vector4 scalarMultiply(double scalar) {
		return new Vector4(this.x * scalar, this.y * scalar, this.z * scalar, this.w * scalar); 
	}
	
	public Vector4 scalarDivision(double scalar) {
		if (scalar != 0) {
			return new Vector4(this.x / scalar, this.y / scalar, this.z / scalar, this.w / scalar);
		}else {
			return this;
		}
	}
	
	public Vector4 normaliser(){
		return this.scalarDivision(this.magnitude());
	}
	
	public Vector4 add(Vector4 vec) {
		return new Vector4(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ(), this.w + vec.getW());
	}
	
	public Vector4 substract(Vector4 vec) {
		vec.negation();
		return this.add(vec);
	}
	
	public double dotProduct(Vector4 vec) {
		return (this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ() + this.w * vec.getW());
	}
	
	public static Matrix vector4ToMatrix(Vector4 vec) {
		Matrix m = new Matrix(4,1);
		m.changeValue( 
				vec.x,
				vec.y,
				vec.z,
				vec.w
				);
		return m;
	}
	
	public static Vector4 matrixToVector4(Matrix m) {
		return new Vector4(m.getComponent().get(0).get(0), m.getComponent().get(1).get(0), m.getComponent().get(2).get(0), m.getComponent().get(3).get(0));
	}
	
	public static Vector3 toVector3(Vector4 vec) {
		return new Vector3(vec.x/vec.w, vec.y/vec.w, vec.z/vec.w);
	}
	
	//GETTER AND SETTER ----------------------------------
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double getW() {
		return this.w;
	}
	
	public int getSize() {
		return 4;
	}

}
