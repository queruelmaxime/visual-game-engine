package mathematics;

public class Point {
	
	//VARIABLES ------------------------------------------
	
	protected double x;
	protected double y;
	protected double z;
	
	protected double r;
	protected double theta;
	protected double phi;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Point() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.r = Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
		this.theta = Math.atan2(this.x, this.z);
		this.phi = Math.asin(-this.y/this.r);
	}
	
	public Point(double m_x, double m_y, double m_z) {
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
		this.r = Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
		this.theta = Math.atan2(this.x, this.z);
		this.phi = Math.asin(-this.y/this.r);
	}
	
	//METHOD ---------------------------------------------
	
	public String toString() {
		return "Point : x = " + this.x + "; y = " + this.y + "; z = " + this.z; 
	}
	
	public Vector3 toVector3() {
		return new Vector3(this.x, this.y, this.z);
	}
	
	public static Point add(Point p0, Point p1) {
		return new Point(p0.x+p1.x,p0.y+p1.y,p0.z+p1.z);
	}
	
	public static Point scalar(Point p, double x) {
		return new Point(p.x*x,p.y*x,p.z*x);
	}
	
	public static Point findMiddle(Point p1, Point p2) {
		return new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2, (p1.getZ()+p2.getZ())/2);
	}
	
	//GETTER AND SETTER ----------------------------------
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getTheta() {
		return theta;
	}

	public void setTheta(double theta) {
		this.theta = theta;
	}

	public double getPhi() {
		return phi;
	}

	public void setPhi(double phi) {
		this.phi = phi;
	}
	

}
