package mathematics;

import java.util.ArrayList;

public class Matrix {
	
	//VARIABLES ------------------------------------------
	
	protected int dimM;
	protected int dimN;
	private ArrayList<ArrayList<Double>> component = new ArrayList<ArrayList<Double>>();
	
	//CONSTRUCTOR ----------------------------------------
	
	public Matrix(int m, int n) {//DIFFERENT SIZE
		this.dimM = m;
		this.dimN = n;
		this.init();
	}
	
	public Matrix(int m) {//SAME SIZE
		this.dimM = m;
		this.dimN = m;
		this.init();
	}
	
	//METHOD ---------------------------------------------
	
	public void init() {//INIT MATRIX COMPONENT
		for (int i = 0; i < dimM; i++) {
			ArrayList<Double> line = new ArrayList<Double>();
			for (int j = 0; j < dimN; j++) {
				line.add(0.0);
			}
			this.getComponent().add(line);
		}
	}
	
	public String toString() {//DISPLAY MATRIX TO STRING
		String str = "";
		for (int i = 0; i < dimM; i++) {
			for (int j = 0; j < dimN; j++) {
				str += this.getComponent().get(i).get(j) + " ";
			}
			String newLine = System.getProperty("line.separator");
			str += newLine;
		}
		return str;
	}
	
	public void changeOneValue(int posX, int posY, double value) {//CHANGE ONE VALUE
		for (int i = 0; i < dimM; i++) {
			for (int j = 0; j < dimN; j++) {
				if (i == posX && j == posY) {
					this.getComponent().get(i).set(j, value);
					break;
				}
			}
		}
	}
	
	public void changeValue(double...values) {//CHANGE ALL VALUES MATRIX
		int inc = 0;
		for (int i = 0; i < dimM; i++) {
			for (int j = 0; j < dimN; j++) {
				this.getComponent().get(i).set(j, values[inc]);
				inc++;
			}
		}
	}
	
	public Vector3 lineVector3(int line) {
		return new Vector3(this.getComponent().get(line).get(0), this.getComponent().get(line).get(1), this.getComponent().get(line).get(2) );
	}
	
	public void combine(Matrix matrix) {//COMBINE 2 MATRIX
		if (dimM == matrix.dimM && dimN == matrix.dimN) {
			for (int i = 0; i < dimM; i++) {
				for (int j = 0; j < dimN; j++) {
						this.getComponent().get(i).set(j, getComponent().get(i).get(j) + matrix.getComponent().get(i).get(j));
				}
			}
		}else {
			System.err.println("ERROR BAD SIZE");
		}
	}
	
	public void scalarMultiply(double scalar) {//MULTIPLY MATRIX AND SCALAR
		for (int i = 0; i < dimM; i++) {
			for (int j = 0; j < dimN; j++) {
					this.getComponent().get(i).set(j, this.getComponent().get(i).get(j)*scalar);
			}
		}
	}
	
	public void substract(Matrix matrix) {//SUBSTRACT 2 MATRIX
		matrix.scalarMultiply(-1);
		this.combine(matrix);
	}
	
	public Matrix multiply(Matrix matrix) {//MULTIPLY 2 MATRIX
		if (dimN != matrix.dimM) {
			System.err.println("ERROR INCOMPATIBLE MATRIX");
			System.exit(1);
			return null;
		}else {
			Matrix mat = new Matrix(dimM, matrix.dimN);
			for (int i = 0; i < mat.dimM; i++) {
				for (int j = 0; j < mat.dimN; j++) {
					double res = 0;
					for (int k = 0; k < dimN; k++) {
						res += this.getComponent().get(i).get(k) * matrix.getComponent().get(k).get(j);	
					}
					mat.getComponent().get(i).set(j, res);
				}
			}
			return mat;
		}
	}
	
	public Vector4 multiplyVector4(Vector4 vec) {
		return Vector4.matrixToVector4(this.multiply(Vector4.vector4ToMatrix(vec)));
	}
	
	public Matrix transpose() {
		Matrix mat = new Matrix(this.getDimM(), this.getDimN());
		for (int i = 0; i < mat.dimM; i++) {
			for (int j = 0; j < mat.dimN; j++) {
				mat.changeOneValue(i, j, this.getComponent().get(j).get(i));
			}
		}
		return mat;
	}
	
	public static Matrix translate(double dx, double dy, double dz) {
		Matrix translate = new Matrix(4,4);
		translate.changeValue(
				1, 0, 0, dx,
				0, 1, 0, dy,
				0, 0, 1, dz,
				0, 0, 0, 1
		);
	return translate;
	}
	
	public static Matrix rotateX(double degree) {
		double theta = 2*Math.PI/360*degree;
		Matrix rotate = new Matrix(4,4);
		rotate.changeValue(
				1, 0, 0, 0, 
				0, Math.cos(theta), -Math.sin(theta), 0,
				0, Math.sin(theta), Math.cos(theta), 0,
				0, 0, 0, 1
		);
	return rotate;
	}
	
	public static Matrix rotateY(double degree) {
		double theta = 2*Math.PI/360*degree;
		Matrix rotate = new Matrix(4,4);
		rotate.changeValue(
				Math.cos(theta), 0, Math.sin(theta), 0,
				0, 1, 0, 0,
				-Math.sin(theta), 0, Math.cos(theta), 0,
				0, 0, 0, 1
		);
	return rotate;
	}
	
	public static Matrix rotateZ(double degree) {
		double theta = 2*Math.PI/360*degree;
		Matrix rotate = new Matrix(4,4);
		rotate.changeValue(
				Math.cos(theta), -Math.sin(theta), 0, 0, 
				Math.sin(theta), Math.cos(theta), 0, 0, 
				0, 0, 1, 0,
				0, 0, 0, 1
		);
	return rotate;
	}
	
	public static Matrix scale(double scaleX, double scaleY, double scaleZ) {
		Matrix scale = new Matrix(4,4);
		scale.changeValue(
				scaleX, 0, 0, 0,
				0, scaleY, 0, 0,
				0, 0, scaleZ, 0,
				0, 0, 0, 1
		);
	return scale;
	}
	
	public double determinant() {
		if (this.dimM == 2 && this.dimN == 2) {
			return this.getComponent().get(0).get(0)*this.getComponent().get(1).get(1) - this.getComponent().get(0).get(1)*this.getComponent().get(1).get(0);
		}else if (this.dimM == 3 && this.dimN == 3) {
			return this.getComponent().get(0).get(0)*this.getComponent().get(1).get(1)*this.getComponent().get(2).get(2) 
					+ this.getComponent().get(0).get(1)*this.getComponent().get(1).get(2)*this.getComponent().get(2).get(0)
					+ this.getComponent().get(0).get(2)*this.getComponent().get(1).get(0)*this.getComponent().get(2).get(1)
					- this.getComponent().get(0).get(2)*this.getComponent().get(1).get(1)*this.getComponent().get(2).get(0)
					- this.getComponent().get(0).get(1)*this.getComponent().get(1).get(0)*this.getComponent().get(2).get(2)
					- this.getComponent().get(0).get(0)*this.getComponent().get(1).get(2)*this.getComponent().get(2).get(1);
		}else if (this.dimM == 4 && this.dimN == 4){
			return (this.getComponent().get(0).get(0)*((this.subMatrix(0,0)).determinant())
					- this.getComponent().get(0).get(1)*((this.subMatrix(0,1)).determinant())
					+ this.getComponent().get(0).get(2)*((this.subMatrix(0,2)).determinant())
					- this.getComponent().get(0).get(3)*((this.subMatrix(0,3)).determinant()));
		}
		else {
			System.err.println("ERROR BAD SIZE");
			return 0;
		}
	}
	
	public Matrix subMatrix(int i, int j) {
		if (dimM == dimN) {
			int newX = 0, newY = 0;
			Matrix matrix = new Matrix(dimM-1);
			for (int k = 0; k < dimM; k++) {
				for (int l = 0; l < dimN; l++) {
					if (k != i && l != j) {
						matrix.changeOneValue(newX, newY, this.getComponent().get(k).get(l));
						newY++;
					}
				}
				if (k != i) {
					newY = 0;
					newX++;
				}
			}
			return matrix;
		}else {
			System.err.println("ERROR BAD SIZE");
			return null;
		}
	}
	
	public Matrix comatrix() {
		if (this.dimM == 2 && this.dimN == 2) {
			Matrix matrix = new Matrix(2);
			matrix.changeValue(
					this.getComponent().get(1).get(1), -this.getComponent().get(1).get(0),
					-this.getComponent().get(0).get(1), this.getComponent().get(0).get(0)
					);
			return matrix;
		}else if (this.dimM == 3 && this.dimN == 3) {
			int sign = -1;
			Matrix matrix = new Matrix(dimM);
			for (int i = 0; i < dimM; i++) { 
				for (int j = 0; j < dimN; j++) {
					sign *= -1;
					Matrix sub = this.subMatrix(i, j);
					matrix.changeOneValue(i, j, sign*sub.determinant());
				}
			}
			return matrix;
		}else if (this.dimM == 4 && this.dimN == 4) {
			Matrix matrix = new Matrix(dimM);
			for (int i = 0; i < dimM; i++) { 
				for (int j = 0; j < dimN; j++) {
					double sign = Math.pow(-1, (i+j));
					Matrix sub = this.subMatrix(i, j);
					matrix.changeOneValue(i, j, sign*sub.determinant());
				}
			}
			return matrix;
		}else {
			System.err.println("ERROR BAD SIZE");
			return null;
		}
	}
	
	public Matrix inverse() {
		Matrix matrix = this.comatrix().transpose();
		matrix.scalarMultiply(1/this.determinant());
		return matrix;
	}
	
	public static Matrix eulerToMatrix(Euler e) {
		double ch = Math.cos(e.roll);
		double sh = Math.sin(e.roll);
		double cp = Math.cos(e.pitch);
		double sp = Math.sin(e.pitch);
		double cb = Math.cos(e.yaw);
		double sb = Math.sin(e.yaw);
		Matrix m = new Matrix(4);
		m.changeValue(
				ch*cb+sh*sp*sb, sb*cp, -sh*cb+ch*sp*sb, 0,
				-ch*sb+sh*sp*cb, cb*cp, sb*sh+ch*sp*cb, 0,
				sh*cp, -sp, ch/cp, 0,
				0,0,0,1
				);
		return m;
	}
	
	
	public static Matrix quaternionToMatrix(Quaternion q) {
		Matrix m = new Matrix(4);
		m.changeValue(
				1-2*q.y*q.y-2*q.z*q.z,
				2*q.x*q.y+2*q.w*q.z,
				2*q.x*q.z-2*q.w*q.y,
				0,
				2*q.x*q.y-2*q.w*q.z,
				1-2*q.x*q.x-2*q.z*q.z,
				2*q.y*q.z+2*q.w*q.x,
				0,
				2*q.x*q.z+2*q.w*q.y,
				2*q.y*q.z-2*q.w*q.x,
				1-2*q.x*q.x-2*q.y*q.y,
				0,
				0,0,0,1);
		return m;
	}
	
	
	//GETTER AND SETTER ----------------------------------
	
	public int getDimM() {
		return dimM;
	}

	public int getDimN() {
		return dimN;
	}

	public ArrayList<ArrayList<Double>> getComponent() {
		return component;
	}

}
