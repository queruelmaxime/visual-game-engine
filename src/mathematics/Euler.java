package mathematics;

public class Euler {
	
	//VARIABLES ------------------------------------------
	
	protected double roll;
	protected double pitch;
	protected double yaw;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Euler() {
		this.roll = 0;
		this.pitch = 0;
		this.yaw = 0;
	}
	
	public Euler(double m_roll, double m_pitch, double m_yaw) {
		this.roll = m_roll;//heading
		this.pitch = m_pitch;
		this.yaw = m_yaw;//bank
	}
	
	//METHOD ---------------------------------------------
	
	public String toString() {
		return "Euler Angle : \n" + 
				"	roll = " + this.roll + "\n" +
				"	pitch = " + this.pitch + "\n" +
				"	yaw = " + this.yaw;
	}
	
	
	public static Euler matrixToEuler(Matrix m) {
		double sp = -Math.asin(-m.getComponent().get(2).get(1));
		double pitch = -Math.asin(sp);
		if (sp <= -1) {
			pitch = -Math.PI/2;
		}if (sp >= 1) {
			pitch = Math.PI/2;
		}
		double heading = Math.atan2(m.getComponent().get(2).get(0)/Math.cos(pitch), m.getComponent().get(2).get(2)/Math.cos(pitch));
		double bank = Math.atan2(m.getComponent().get(0).get(1)/Math.cos(pitch), m.getComponent().get(1).get(1)/Math.cos(pitch));
		return new Euler(heading,pitch,bank);
	}
	
	public static Euler quaternionToEuler(Quaternion q1) {
		Matrix tmp = Matrix.quaternionToMatrix(q1);
		return Euler.matrixToEuler(tmp);
	}
	
	public static Euler LERP(Euler location, Euler origin, double beta) {
		
		Euler lerp = new Euler((1-beta)*location.getRoll() + beta * origin.getRoll(),
				(1-beta)*location.getPitch() + beta * origin.getPitch(),
				(1-beta)*location.getYaw() + beta * origin.getYaw()
				);
		return lerp;
	}
	
	//GETTER AND SETTER ----------------------------------
	
	public double getRoll() {
		return roll;
	}
	public void setRoll(double roll) {
		this.roll = roll;
	}
	public double getPitch() {
		return pitch;
	}
	public void setPitch(double pitch) {
		this.pitch = pitch;
	}
	public double getYaw() {
		return yaw;
	}
	public void setYaw(double yaw) {
		this.yaw = yaw;
	}
	
	

}
