package mathematics;

public class Vector3 {
	
	//VARIABLES ------------------------------------------
	
	protected double x;
	protected double y;
	protected double z;
	
	//CONSTRUCTOR ----------------------------------------
	
	public Vector3() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public Vector3(double m_x, double m_y, double m_z) {
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
	}
	
	
	//METHOD ---------------------------------------------
	
	public String toString() {//TP1
		return "Vector 3 : <" + this.x + "," + this.y + "," + this.z + ">";
	}
	
	public void changeValue(double m_x, double m_y, double m_z) { //CHANGE ALL VALUE
		this.x = m_x;
		this.y = m_y;
		this.z = m_z;
	}
	
	public void negation() {//TP1
		this.changeValue(-this.x, -this.y, -this.z);
	}
	
	public double magnitude() { //TP1
		return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
	}
	
	public Vector3 scalarMultiply(double scalar) {//TP1
		return new Vector3(this.x * scalar, this.y * scalar, this.z * scalar); 
	}
	
	public Vector3 scalarDivision(double scalar) {
		if (scalar != 0) {
			return new Vector3(this.x / scalar, this.y / scalar, this.z / scalar);
		}else {
			return this;
		}
	}
	
	public Vector3 normaliser(){//TP1
		double m = this.magnitude();
		this.setX(this.getX()/m);
		this.setY(this.getY()/m);
		this.setZ(this.getZ()/m);
		return this;
	}

	public Vector3 add(Vector3 vec) {//TP1
		return new Vector3(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
	}
	
	public Vector3 substract(Vector3 vec) {//TP1
		return new Vector3(this.x-vec.getX(), this.y-vec.getY(), this.z-vec.getZ());
	}
	
	public static double distance(Point a, Point b) {
		Vector3 vec = a.toVector3();
		vec.substract(b.toVector3());
		return vec.magnitude();
	}
	
	public double dotProduct(Vector3 vec) {//TP1
		return (this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ());
	}
	
	public Vector3 crossProduct(Vector3 vec) {//TP1
		return new Vector3((this.y*vec.getZ() - this.z*vec.getY()),(this.z*vec.getX() - this.x*vec.getZ()),(this.x*vec.getY() - this.y*vec.getX()));
	}
	
	public Vector3 inverse() {
		return new Vector3(1/this.x, 1/this.y, 1/this.z);
	}
	
	public static Point vector3ToPoint(Vector3 vec) {
		return new Point(vec.x, vec.y, vec.z);
	}
	
	public static Vector4 toVector4(Vector3 vec) {
		return new Vector4(vec.x, vec.y, vec.z, 1);
	}
	
	//GETTER AND SETTER ----------------------------------
	
	public Vector3 getVector3() {
		return this;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public int getSize() {
		return 3;
	}
	
	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZ(double z) {
		this.z = z;
	}
}
