Commande du moteur 3D :

Fl�che/Z/S -> D�placement Cam�ra
ESPACE -> Reset Cam�ra

1/2/3/4/5 -> Diff�rentes Sc�nes
0 -> Algorithme du Peintre par ordered seed fill

P/M -> Taille de l'objet
L -> Lumi�re

NUMPAD 1 -> Wireframe
NUMPAD 2 -> LERP
NUMPAD 3 -> SLERP
NUMPAD 4/5/6 -> Rotation Cam�ra
NUMPAD 7/8/9 -> Rotation Objet